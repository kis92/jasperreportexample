package com;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import java.io.File;
import java.util.*;

public class Main {

    public static void main(String[] args) throws JRException {
        Main main = new Main();
        ClassLoader classLoader = main.getClass().getClassLoader();
        List<User> users = new ArrayList<>();
        Date now = new Date(114, 6, 27);
        for (int i = 0; i < 10; i++) {
            Date tmp = new Date(now.getTime());
            tmp.setMonth(i);
            users.add(new User("user" + i, tmp, i * i * 1000 + 0.5));
        }

        String pathForPattern = "Blank_A4.jrxml";

        String pathForSavingPdf = "TestResult.pdf";
        String pathForSavingXls = "TestResult.xlsx";


        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(users);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("DATE", new Date());
        File reportPattern = new File(classLoader.getResource(pathForPattern).getFile());
        JasperDesign jasperDesign = JRXmlLoader.load(reportPattern);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
                parameters, beanColDataSource);

        JasperExportManager.exportReportToPdfFile(jasperPrint, pathForSavingPdf);


        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
        exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathForSavingXls);
        exporter.exportReport();

    }
}
