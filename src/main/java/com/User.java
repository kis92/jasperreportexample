package com;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user on 19.01.17.
 */
public class User {
    private String name;
    private Date birthDate;
    private double salary;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");

    public User(String name, Date birthDate, double salary) {
        this.name = name;
        this.birthDate = birthDate;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return dateFormat.format(birthDate);
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"," +
                "\"birthDate\":\"" + dateFormat.format(birthDate) +"\","+
                "\"salary\":\"" + salary + "\"" +
                '}';
    }
}
